#!/usr/bin/env python3

"""
image-checksums.py

Download, validate and calculate sha256 checksum for Glance images.

Read input Glance images declarations from *-image.yaml files defined by parameter --image-declarations-folder/-d <DECLARATIONS_FOLDER>.

Schema of the declared image should contain at minimum following keys:
  # cirros-0-x86_64-image.yaml
  name: cirros-0-x86_64
  image_source_url: http://download.cirros-cloud.net/0.6.2/cirros-0.6.2-x86_64-disk.img
  image_source_checksum_url: http://download.cirros-cloud.net/0.6.2/MD5SUMS
  image_source_checksum_type: md5


Usage:
  ./image_checksums.py --image-declarations-folder ./prod-brno-cloud-entities.git/environments/prod-brno/openstack/images
  cat CHECKSUMS
"""

import re
import sys
import os
import glob
import hashlib
import argparse
import urllib.request

import yaml

# local contants ------------------------------------------------------------
HASH_CALCULATION_BUFFER_SIZE = 1024 * 1024

# local functions -----------------------------------------------------------
def obtain_url_checksum_file(url):
    """Function obtains URL checksum file for comparison"""
    try:
        with urllib.request.urlopen(url) as f:
            all_upstream_checksums = f.readlines()
    except (urllib.error.HTTPError, urllib.error.URLError) as e:
        print(f"ERROR: Unable to retrieve upstrem checksum file from {url}:")
        print(str(e), file=sys.stderr)
        raise e

    checksum_file_no_comments_content = [
        i_record.rstrip().decode()
        for i_record in all_upstream_checksums
        if not i_record.startswith(b"#")
    ]

    return checksum_file_no_comments_content


def calculate_checksum(image_file_path, checksum_algo_type):
    """Calculates checksum for specified file and algorithm"""
    checksum_calculator = getattr(hashlib, checksum_algo_type)()

    try:
        with open(image_file_path, "rb") as f:
            for byte_chunks in iter(lambda: f.read(HASH_CALCULATION_BUFFER_SIZE), b""):
                checksum_calculator.update(byte_chunks)
    except OSError as read_exception:
        print(str(read_exception), file=sys.stderr)
        raise read_exception

    return checksum_calculator.hexdigest()


def verify_image_file_integrity(
    image_file_path, checksum_verification_file, checksum_algo_type
):
    """Verifies downloaded image file integrity"""
    is_image_file_verified = False
    calculated_image_file_checksum = calculate_checksum(
        image_file_path, checksum_algo_type
    )

    for line in checksum_verification_file:
        if re.search(calculated_image_file_checksum, line):
            is_image_file_verified = True
            break

    assert is_image_file_verified, f"ERROR: Did not find any supported checksum pattern match for {os.path.basename(image_file_path)} in online repository"


def calculate_sha256_checksum(image_file_path):
    """Calculates SHA256 checksum for specified file"""
    return calculate_checksum(image_file_path, "sha256")


def download_image_file(url, file_path):
    """check image content validity, compute MD5 checksum"""
    try:
        urllib.request.urlretrieve(url, file_path)
    except (urllib.error.HTTPError, urllib.error.URLError) as e:
        print(f"ERROR: Unable to retrieve upstrem checksum file from {url}:")
        print(str(e), file=sys.stderr)
        raise e


def read_declarations(image_declarations_folder):
    """Reads yaml declarations of OpenStack files"""

    declaration_files = []

    supported_files = ["**/*.yaml", "**/*.yml"]

    for i_supported_file in supported_files:
        declaration_files += glob.glob(
            os.path.join(image_declarations_folder, i_supported_file), recursive=True
        )
    image_declarations = []

    try:
        for i_declaration_file in declaration_files:
            with open(i_declaration_file, "r", encoding="utf-8") as fh:
                file_content = yaml.safe_load(fh) or {}
                file_content['__file__'] = i_declaration_file
                image_declarations.append(file_content)
                print(f"INFO: Image declaration sucessfully loaded from {i_declaration_file}.")

    except yaml.YAMLError as yaml_exception:
        # Something went wrong during reading
        print(str(yaml_exception), file=sys.stderr)
        raise yaml_exception

    return image_declarations


def get_unique_image_download_metadata(image_declarations):
    """Gets unique image download metadata"""
    unique_image_download_metadata = {}

    for i_declaration in image_declarations:
        if ("image_source_url" in i_declaration
            and "image_source_checksum_url" in i_declaration
            and "image_source_checksum_type" in i_declaration):
            if i_declaration["image_source_url"] not in unique_image_download_metadata:
                unique_image_download_metadata[i_declaration["image_source_url"]] = {
                    "url": i_declaration["image_source_url"],
                    "checksum_url": i_declaration["image_source_checksum_url"],
                    "checksum_algo_type": i_declaration["image_source_checksum_type"],
                }
        else:
            print(f"WARNING: Image declaration in {i_declaration.get('__file__')} file is skipped as it does not follow expected schema.")

    return unique_image_download_metadata


def calculate_output_image_checksums_file(image_download_metadata):
    """Downloads image files, checksums, verifies them and calculates SHA256 checksum of downloaded file"""

    calculated_checksums = {}

    for i_image_download_metadata_item in image_download_metadata.values():

        # Downloading image file
        downloaded_image_file_path = os.path.join(
            "/tmp", os.path.basename(i_image_download_metadata_item["url"])
        )
        download_image_file(
            i_image_download_metadata_item["url"], downloaded_image_file_path
        )
        # Downloading image checksum file
        checksum_file = obtain_url_checksum_file(
            i_image_download_metadata_item["checksum_url"]
        )
        verify_image_file_integrity(
            downloaded_image_file_path,
            checksum_file,
            i_image_download_metadata_item["checksum_algo_type"],
        )
        calculated_checksums[
            i_image_download_metadata_item["url"]
        ] = calculate_sha256_checksum(downloaded_image_file_path)

    return calculated_checksums


def write_checksums(output_checksum_file, checksums):
    """Writes calculated checksums"""
    try:
        with open(output_checksum_file, "w", encoding="utf-8") as fh:
            fh.write(
                "\n".join(
                    [
                        f"{i_image_url} {i_calculated_checksum}"
                        for i_image_url, i_calculated_checksum in checksums.items()
                    ]
                )
            )
        print("INFO: Checksum file generated as %s." % output_checksum_file)
    except OSError as write_exception:
        print(str(write_exception), file=sys.stderr)
        raise write_exception


if __name__ == "__main__":
    PARSER = argparse.ArgumentParser()

    PARSER.add_argument(
        "-d", "--image-declarations-folder",
        required=True,
        help="OpenStack image yaml declarations folder path",
    )
    PARSER.add_argument(
        "-f", "--output-checksum-file",
        default="CHECKSUMS",
        help="File to be published with Gitlab pages containing SHA256 checksums of most upstream images from declarations",
    )

    ARGS = PARSER.parse_args()

    IMAGE_DECLARATIONS = read_declarations(ARGS.image_declarations_folder)

    UNIQUE_IMAGE_DOWNLOAD_METADATA = get_unique_image_download_metadata(
        IMAGE_DECLARATIONS
    )


    if UNIQUE_IMAGE_DOWNLOAD_METADATA:
        print(
            "INFO: Plan to compute image freshness checksums for following image urls %s."
            % [
                UNIQUE_IMAGE_DOWNLOAD_METADATA[i]["url"]
                for i in UNIQUE_IMAGE_DOWNLOAD_METADATA
            ]
        )

        CALCULATED_CHECKSUMS = calculate_output_image_checksums_file(
            UNIQUE_IMAGE_DOWNLOAD_METADATA
        )

        write_checksums(ARGS.output_checksum_file, CALCULATED_CHECKSUMS)
